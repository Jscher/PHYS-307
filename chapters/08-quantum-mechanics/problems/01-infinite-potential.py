import numpy as np
import matplotlib.pyplot as plt
import math
import random

fig, axs = plt.subplots(1)

L =1
dL =.1
N =1

n = 1
V = 1
even_psi = [ 1, 1 ]
#odd_psi = [ 0 0 ]

E = np.pi**2 /2

k_p = (2 * n - 1)/(2 * L) * np.pi
k_m = 2 * np.pi / L


def update_wavefunction(psi, l):
    psi_n = psi[-1]
    psi_n1 = psi[-2]
    psi.append(2 * psi_n - psi_n1 - 2 * (dL ** 2) * (E - V) * psi_n)
    return psi if l>= L + .5 else update_wavefunction(psi, l+dL)

even_psi=update_wavefunction(even_psi, 0)
x_arr= [i * dL for i in range(len(even_psi))]

plt.plot(x_arr, even_psi)

#fig.suptitle('Poincare Section', fontsize=18)
#
#plt.xlabel('Temperature', fontsize=12)
#plt.ylabel('Average Spin', fontsize=12)

#
plt.axhline(y=0, color='r', linestyle='-')
plt.axvline(x=L, color='r', linestyle='-')
plt.show()
