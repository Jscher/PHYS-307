import numpy as np
import matplotlib.pyplot as plt
import random

fig, axs = plt.subplots(1)

## Constants ##
k_B=1
J=1

## Temperature Variables ##
T_0=1
T_inc=1
T_max=50

## Array Parameters ##
ising_arr=[]
s_ave_arr=[]
L=10
H=1
u=1

def new_ising_arr():
    for i in range(100):
        rand_spin = random.random()
        if rand_spin > .5:
            ising_arr.append(1)
        else:
            ising_arr.append(-1)
    

def get_avg_s():
    average = 0
    for i in range(len(ising_arr)):
        average += ising_arr[i]
    return average/len(ising_arr)

def update_spin(s_1, s_2, s_3):
    temp_1 = s_1 * s_2
    temp_2 = s_2 * s_3
    Eflip = -J *  temp_1 - u * H * s_2
    if Eflip < 0:
        s_2 *= -1
    else:
        random_comparison = random.random()
        boltz = np.exp((Eflip * T_0)/(k_B)) 
        if (boltz > random_comparison):
            s_2 *= -1
    Eflip = -J *  temp_2 - u * H * s_2
    if Eflip < 0:
        s_2 *= -1
    else:
        random_comparison = random.random()
        boltz = np.exp((Eflip * T_0)/(k_B)) 
        if (boltz > random_comparison):
            s_2 *= -1

#def calc_avg_s():

def iterate():
    for i in range(T_0, T_max, T_inc):
        new_ising_arr()
        for z in range(100):
            for j in range(len(ising_arr)):
                if(j < len(ising_arr) - 1):
                    update_spin(j - 1, j, j+1)
                else:
                    update_spin(j-1, j, ising_arr[0])
        s_ave_arr.append(get_avg_s())

iterate()
print(s_ave_arr)

plt.plot(s_ave_arr)
# plt.grid(color='grey', linestyle='--', linewidth=.5)
# plt.axhline(0, color='black', linewidth=.5)
# plt.axvline(0, color='black', linewidth=.5)
# plt.plot()

plt.show()
