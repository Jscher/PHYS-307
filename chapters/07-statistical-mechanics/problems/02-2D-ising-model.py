import sys, math, random
import numpy as np
import matplotlib.pyplot as plt

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10**9)

## Constants ##
k_B, J = [ 1, 1 ]

## Temperature Variables ##
T_min, T_max = [ 0, 10 ]
n_T = 100

T_scale = T_max / n_T

## Initialize Temperature Array ##
T_arr = []
for T in range(0, n_T + 1):
    T_arr.append(T * T_max / n_T)

## Iterations and Dimensions
N, L = [ 10, 10 ]

## Magnetic Field Variables ##
mu, H = [ 1, 1 ]

## Spin Arrays ##
avg_spin = []
avg_magnetization = []

## Initialize Spin Arrays and Matrices##
#spin_matrix = []
spin_matrix = np.random.randint(2, size=(10,10))
spin_matrix = spin_matrix * 2 - 1

def rand_spin():
    return np.random.rand()

def new_spin_matrix():
    spin_matrix = np.random.randint(2, size=(10,10))
    #for i in range(len(spin_arr)):
    #    for j in range(len(spin_arr)):
    #        j = -1 if j < .5  else 1 
    spin_matrix = spin_matrix * 2 - 1
    
def update_spin(s_i, s_j, T):
    eflip = -1 * J * s_i * s_j - H * mu * s_i
    boltz = np.exp(eflip / k_B * T)
    return s_i * -1 if eflip < 0 and rand_spin() < boltz else s_i

def parse_spin_matrix(n,m,T):
    for i in range(n):
        for j in range(m):
            cur = spin_matrix[i, j]
            top = spin_matrix[(i - 1) % 10, j]
            bot = spin_matrix[(i + 1) % 10, j]
            lef = spin_matrix[i, (j - 1) % 10]
            rig = spin_matrix[i, (j + 1) % 10]
            spin_matrix[i,j] = update_spin(cur,lef, T)
            spin_matrix[i,j] = update_spin(cur,rig, T)
            spin_matrix[i,j] = update_spin(cur,top, T)
            spin_matrix[i,j] = update_spin(cur,bot, T)

def get_partners(p_1):
    pass


#new_spin_matrix()
print(spin_matrix)
for i in T_arr:
    parse_spin_matrix(10,10,i)
    avg_spin.append(np.mean(spin_matrix))

fig.suptitle('Average spin vs. Temperature', fontsize=18)
plt.plot(T_arr, avg_spin)

plt.xlabel('Temperature', fontsize=12)
plt.ylabel('Average Spin', fontsize=12)

plt.show()
