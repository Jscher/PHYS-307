import sys
import numpy as np
import matplotlib.pyplot as plt
from random import randrange

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10**9)

PI = np.pi
dt = 0.002
total_time = 1

r_0 = (1,0)
v_0 = (0,PI)

r_n=[r_0]
v_n=[v_0]

def orbit(time_remaining):

    (x,y) = r_n[-1]; (v_x, v_y) = v_n[-1]

    time_remaining = time_remaining - dt
    r = np.sqrt(x**2 + y**2)

    v_x = v_x - (4 * PI**2 * x)/r**3 * dt
    v_y = v_y - (4 * PI**2 * y)/r**3 * dt

    x = x + v_x * dt
    y = y + v_y * dt

    r_n.append( (x,y) )
    v_n.append( (v_x,v_y) )

    return True if time_remaining <= 0 else orbit(time_remaining)

orbit(total_time)

plt.grid(color='grey', linestyle='--', linewidth=.5)
plt.axhline(0, color='black', linewidth=.5)
plt.axvline(0, color='black', linewidth=.5)
plt.plot(*zip(*r_n))

print(r_n)

print('---------')
print(r_n)

plt.show()
