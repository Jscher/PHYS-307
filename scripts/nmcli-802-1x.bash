#!/usr/bin/env bash


print_selection() {

	echo \#\# SSID
	i=-1
	nmcli device wifi | head -11 | sed -E 's/^\ *(.*)I.*$/\1/g' | sed -E 's/^[A-Z0-9:]+ //g' | while read -r LINE; do
		i=$((i+1))
		test $i -eq 0  && continue
		[ $i -lt 10 ] && NUM=0$i || NUM=$i
		echo $NUM $LINE
	done
}

get_bssid(){
	NUM=$1
	if [[ ! $NUM =~ ^[0-9]+$ ]] || [[ $NUM -gt 10 ]] || [[ $NUM -lt 1 ]]; then get_bssid; fi

	nmcli device wifi | head -11 | sed -E 's/^\ *(.*)I.*$/\1/g' | sed -E 's/\ .*$//g' | while read -r LINE; do
		if [[ "$i" -eq "$NUM" ]]; then
			echo $LINE
		fi
		i=$((i+1))
	done
}

main () {
	clear
	print_selection
	printf 'Choose Network Number: '
	read -r NUM
	if [[ ! $NUM =~ ^[0-9]+$ ]] || [[ $NUM -gt 10 ]] || [[ $NUM -lt 1 ]]; then main; fi
	BSSID=$(get_bssid $NUM)

	printf 'Enter Username: '
	read -r USERNAME

	printf 'Enter Password: '
	read -r PASSWORD

	nmcli connection modify "$BSSID" 802-1x.identity "$USERNAME" 802-1x.password "$PASSWORD"
	nmcli device wifi connect "$BSSID"

}

main
