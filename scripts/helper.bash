#!/usr/bin/env bash

SCRIPT_SOURCE=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_SOURCE" || exit; cd ../;

SUBMODULE_CONFIG=$(grep -A 2 'submodule' .gitmodules )
SUBMODULE_PATHS=( $(sed -n 's/\[submodule "\(.*\)"\]$/\1/p' .gitmodules) )

REPO_NAME=$(grep -A 1 'remote "origin"' .git/config | sed -n 's/.*url.*\/\(.*\)/\1/p')

NAME=$(whoami)

#echo "${SUBMODULE_CONFIG[@]}"

main(){
    grep -q "gitlab" "$HOME/.gitconfig" || _set_glab_repo_user
    GLAB_REPO_USER=$(git config --get gitlab.user)
    #_recurse_submodules
    _gen_main_config
}

function _gen_main_config {
    (_gen_git_conf_head
     _gen_upstream ".git/config"
     _gen_new_origin
     _gen_main) | tee > a_file-$(date +%s).bak

}


function _recurse_submodules {

    for SUBMODULE in "${SUBMODULE_PATHS[@]}"; do
        SUB_FULL_PATH=".git/modules/$SUBMODULE/config"
        (_gen_submodule_head
        _gen_upstream
        _gen_new_origin
        _gen_main) | tee > a_file-$(date +%s).bak
        #_gen_new_origin $SUBMODULE
        #_gen



        break
    done

}

function _set_glab_repo_user {
    clear;
    printf "Set Gitlab User Name\n"
    printf "Press 'enter' to Accept Default: %s\n:" "$NAME"
    read -r REPO_USER
    [[ -n "$REPO_USER" ]] && NAME="$REPO_USER"
    git config --global gitlab.user "$NAME"
}

function _gen_upstream {
    REPO_PATH="$1"
    grep  -A 2 "remote \"origin\"" "$REPO_PATH" \
        | sed -e 's/origin/upstream/g' -e 's/\t/  /'
}

function _gen_new_origin {
    printf "[remote \"origin\"]\n"
    printf "  active = true\n"
    printf "  url = git@gitlab.com:"
    printf "%s/%s\n" "$GLAB_REPO_USER" "$REPO_NAME"
}

function _gen_git_conf_head {
    printf "[core]\n"
    printf "  repositoryformatversion = 0\n"
    printf "  filemode = true\n"
    printf "  bare = false\n"
    printf "  logallrefupdates = true\n"
    printf "  fetch = +refs/heads/*:refs/remotes/origin/*\n"
    printf "[branch \"main\"]\n"
    printf "  remote = origin\n"
    printf "  merge = refs/head/main\n"
}

function _gen_submodule_head {
    printf "[core]\n"
    printf "  repositoryformatversion = 0\n"
    printf "  filemode = true\n"
    printf "  bare = false\n"
    printf "  logallrefupdates = true\n"
    printf "  worktree = ../../../../$SUBMODULE\n"
}

function _gen_main {
    printf "[main]\n"
    printf "  remote = origin\n"
    printf "  merge = refs/heads/main\n"
}


main
