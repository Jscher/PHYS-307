#!/usr/bin/env bash

command -v gst-launch-1.0 || sudo pacman -S gstreamer
gst-launch-1.0 -e -v udpsrc port=50001 \
                   ! application/x-rtp, encoding-name=JPEG,payload=26 \
                   ! rtpjpegdepay \
                   ! jpegdec \
                   ! autovideosink

# gst-launch-1.0 udpsrc port="50001"  \
#              ! application/x-rtp,\
#                            encoding-name=JPEG,\
#                            payload=30 \
#              ! rtpjpegdepay ! jpegdec ! autovideosink & disown
